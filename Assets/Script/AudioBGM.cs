﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBGM : MonoBehaviour
{
    private string audioPath = "BGM/";
    private const string bgm_gameplay = "BG_Music";
    private AudioClip bgm_clip;
    private AudioSource bgm_source;
    bool play = true;
    public static AudioBGM instance = null;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);
        bgm_source = GetComponent<AudioSource>();
        LoadSound();
    }
    void Start()
    {
        PlayBGM();
    }
    void LoadSound()
    {
        bgm_clip = Resources.Load<AudioClip>(audioPath + bgm_gameplay);
    }
    void PlayBGM()
    {
        bgm_source.Play();
    }
    void StopBGM()
    {
        bgm_source.Stop();
    }
}
