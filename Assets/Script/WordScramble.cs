﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

#region class word
[System.Serializable]
public class Word
{
    public string word;
    [Header("leave empty if you want to randomized")]
    public string desiredRandom;

    public string GetString ()
    {
        if (!string.IsNullOrEmpty(desiredRandom))
        {
            return desiredRandom;
        }

        string result = word;
        

        while (result == word)
        {
            result = "";
            List<char> characters = new List<char>(word.ToCharArray());
            while (characters.Count > 0)
            {
                int indexChar = Random.Range(0, characters.Count - 1);
                result += characters[indexChar];

                characters.RemoveAt(indexChar);
            }
        }
        return result;
    }
}
#endregion

public class WordScramble : MonoBehaviour
{
    public Word[] words;

    [Header("UI REFERENCE")]
    public CharObject prefab;
    public Transform container;
    public float space;
    public float lerpSpeed = 5f;
    public float waitChecking = 3f;

    private int movements;
    public Text moveCount;

    List<CharObject> charObjects = new List<CharObject>();
    CharObject firstSelected;

    public static int currentWord;
    public static string thisWord;
    public static string thisScramble;
    public static WordScramble main;

    public Image image;
    public static int category;

    private bool limitQuestion = false;

    private void Awake()
    {
        main = this;
    }

    void ButtonListener(){
        reward_pnl.SetActive(false);
        pause_pnl.SetActive(false);
  
        next_btn.onClick.AddListener(()=>{
            AudioFX.instance.PlayBtnFX();
            reward_pnl.SetActive(false);
            movements = 0;
            if (limitQuestion)
            {
                limitQuestion = false;
                SceneManager.LoadScene("MenuScene");
            } else
                ShowScramble(currentWord);
        });

        pause_btn.onClick.AddListener(() =>
        {
            AudioFX.instance.PlayBtnFX();
            PausePnlActive();
        });
        resume_btn.onClick.AddListener(() =>
        {
            pause_pnl.SetActive(false);
            Time.timeScale = 1;
            AudioFX.instance.PlayBtnPanelFX();
        });
        mainMenu_btn.onClick.AddListener(() =>
        {
            AudioFX.instance.PlayBtnFX();
            Time.timeScale = 1;
            SceneManager.LoadScene("MenuScene");
        });
    }

    // Start is called before the first frame update
    void Start()
    {
        ButtonListener();
        movements = 0;
        moveCount.text = "Langkah : " + movements;
        ScrambleCategory(category);
        ShowScramble(currentWord);

        index1 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //ScanWord();

        RepositionObject();
        moveCount.text = "Langkah : " + movements;
    }

    void RepositionObject()
    {
        if (charObjects.Count == 0)
        {
            return;
        }

        float center = ((charObjects.Count - 1) / 2);
        for (int i = 0; i < charObjects.Count; i++)
        {
            charObjects[i].rectTransform.anchoredPosition = 
                Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition, 
                new Vector2((i - center) * space, 0), lerpSpeed * Time.deltaTime);
            charObjects[i].index = i;
        }
    }

    // show random word to the screen
    public void ShowScramble()
    {
        ShowScramble(Random.Range(0, words.Length - 1));
    }

    // show random word to the screen
    public void ScrambleCategory(int wordCategory)
    {
        if (wordCategory == 1)
        {
            currentWord = 6;
        } 
        else
        {
            currentWord = 0;
        }
    }

    // show word from collection with certain index
    public void ShowScramble (int index)
    {
        charObjects.Clear();
        foreach(Transform child in container)
        {
            Destroy(child.gameObject);
        }

        if (index > words.Length - 1)
        {
            Debug.LogError("Index out of range, please enter range between 0 - " +
                (words.Length - 1).ToString());
            limitQuestion = true;
            LastRewardPnlActive();
        }

        char[] chars = words[index].GetString().ToCharArray();
        foreach (char c in chars)
        {
            CharObject clone = Instantiate(prefab.gameObject).GetComponent<CharObject>();
            clone.transform.SetParent(container);

            charObjects.Add(clone.Init(c));
        }

        currentWord = index;
        ChangeImage();

        //CoArrangeWord();
        index1 = 0;
        // ScanWord();

        thisWord = words[currentWord].word;
        thisScramble = words[currentWord].GetString();

    }

    public void Swap (int indexA, int indexB)
    {
        CharObject tempA = charObjects[indexA];

        charObjects[indexA] = charObjects[indexB];
        charObjects[indexB] = tempA;

        charObjects[indexA].transform.SetAsLastSibling();
        charObjects[indexB].transform.SetAsLastSibling();

        movements++;

        CheckWord();
    }

    public void Select (CharObject charObject)
    {
        if (firstSelected)
        {
            Swap(firstSelected.index, charObject.index);
            //movements += 1;

            //unselect
            firstSelected.Select();
            charObject.Select();

        } else
        {
            firstSelected = charObject;
            
        }
        
    }

    public void UnSelect()
    {
        firstSelected = null;
    }

    public void CheckWord()
    {
        StartCoroutine(CoCheckWord()); 
    }

    IEnumerator CoCheckWord()
    {
        yield return new WaitForSeconds(waitChecking);

        string word = "";
        foreach (CharObject charObject in charObjects)
        {
            word += charObject.character;
        }

        if (word == words[currentWord].word)
        {
            if (currentWord == 5)
            {
                Debug.Log("Index Fruit and Veggie reached limit");
                limitQuestion = true;
                LastRewardPnlActive();
            }
            else
            {
                currentWord++;
                // ShowScramble(currentWord);
                RewardPnlActive();
                // movements = 0;
            }
        } else
        {
            if (index1 < words[currentWord].word.Length)
            {
                index1++;
            } else
            {
                index1 = 0;
            }

            //CoArrangeWord();
            // ScanWord();
        }
    }

    void ChangeImage()
    {
        if (category == 1)
        {
            image.sprite = Resources.Load<Sprite>("Hewan/" + words[currentWord].word);
        }
        else
        {
            image.sprite = Resources.Load<Sprite>("Buah/" + words[currentWord].word);
        }
        Debug.Log(words[currentWord].word);
    }

    private void AIScanner()
    {
        string Scramble = "";

        foreach (CharObject charObject in charObjects)
        {
            Scramble += charObject.character;
        }

    }

    public void ScanWord()
    {
        StartCoroutine(CoArrangeWord());
    }

    private int index1;
    IEnumerator CoArrangeWord()
    {
        yield return new WaitForSeconds(3f);

        string Scramble = "";

        foreach (CharObject charObject in charObjects)
        {
            Scramble += charObject.character;
        }

        Debug.Log("Scramble : " + Scramble);

        char[] charsScramble = Scramble.ToCharArray();
        char[] charsResult = words[currentWord].word.ToCharArray();

        for (int i = index1; i < charsResult.Length; i++)
        {
            if (charsScramble[i] == charsResult[index1])
            {
                if (index1 != i)
                {
                    if (charsScramble[index1] != charsScramble[i])
                    {
                        Swap(index1, i);
                        Scramble = "";
                        foreach (CharObject charObject in charObjects)
                        {
                            Scramble += charObject.character;
                        }
                        Debug.Log(Scramble);
                        break;
                    }  
                } else
                {
                    index1++;
                    ScanWord();
                    break;
                }

            }
        }
    }
    #region Reward Panel
    [SerializeField]
    private GameObject reward_pnl;
    [SerializeField]
    private Button next_btn;
    [SerializeField]
    private Text nextBtn_txt;
    [SerializeField]
    private Text step_txt;
    public void RewardPnlActive(){
        AudioFX.instance.PlayBtnPanelFX();
        reward_pnl.SetActive(true);
        nextBtn_txt.text = "Lanjut";
        step_txt.text = "Langkah : " + movements;
    }
    public void LastRewardPnlActive()
    {
        AudioFX.instance.PlayBtnPanelFX();
        reward_pnl.SetActive(true);
        nextBtn_txt.text = "Menu\nUtama";
        step_txt.text = "Selesai";
    }
    #endregion
    #region Pause Panel
    [SerializeField]
    private Button pause_btn;
    [SerializeField]
    private GameObject pause_pnl;
    [SerializeField]
    private Button resume_btn;
    [SerializeField]
    private Button mainMenu_btn;
    public void PausePnlActive()
    {
        AudioFX.instance.PlayBtnPanelFX();
        pause_pnl.SetActive(true);
        Time.timeScale = 0;
    }
    #endregion
}

