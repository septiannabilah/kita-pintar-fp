﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFX : MonoBehaviour
{
    private string audioPath = "SFX/";
    private const string clickBtn_sound = "sound_click-btn";
    private const string clickBtnPanel_sound = "sound_click-btn-panel";
    private AudioClip clickBtn_clip;
    private AudioClip clickBtnPanel_clip;
    bool play = true;
    private AudioSource soundFX;
    public static AudioFX instance = null;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
            instance = this;
        DontDestroyOnLoad(this.gameObject);

        LoadSound();
    }
    void Start()
    {
        soundFX = GetComponent<AudioSource>();
    }

    void LoadSound()
    {
        clickBtn_clip = Resources.Load<AudioClip>(audioPath + clickBtn_sound);
        clickBtnPanel_clip = Resources.Load<AudioClip>(audioPath + clickBtnPanel_sound);
    }
    void PlaySFX()
    {
        soundFX.Play();
    }
    void StopSFX()
    {
        soundFX.Stop();
    }
    void PauseSFX()
    {
        if (play)
        {
            soundFX.mute = true;
            play = false;
        }
        else
        {
            soundFX.mute = false;
            play = true;
        }
    }
    public void PlayBtnFX()
    {
        soundFX.clip = clickBtn_clip;
        PlaySFX();
    }
    public void PlayBtnPanelFX()
    {
        soundFX.clip = clickBtnPanel_clip;
        PlaySFX();
    }
}
