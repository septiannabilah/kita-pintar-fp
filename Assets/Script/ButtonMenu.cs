﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonMenu : MonoBehaviour
{
    public GameObject playPanel;

    private void Start()
    {
        playPanel.SetActive(false);
    }

    public void PlayButton()
    {
        AudioFX.instance.PlayBtnFX();
        playPanel.SetActive(true);
    }

    public void ExitButton()
    {
        AudioFX.instance.PlayBtnFX();
        Application.Quit();
        Debug.Log("Quit App");
    }

    public void HewanButton()
    {
        AudioFX.instance.PlayBtnFX();
        WordScramble.category = 1;
        SceneManager.LoadScene("SampleScene");
    }

    public void VeggieButton()
    {
        AudioFX.instance.PlayBtnFX();
        WordScramble.category = 2;
        SceneManager.LoadScene("SampleScene");
    }

}
