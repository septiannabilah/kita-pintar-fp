﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AIBehaviour : MonoBehaviour
{
    public Word[] words;

    [Header("UI REFERENCE")]
    public CharObject prefab;
    public Transform container;
    public float space;
    public float lerpSpeed = 5f;
    public float waitChecking = 3f;

    private int movements;
    public Text moveCount;

    List<CharObject> charObjects = new List<CharObject>();
    CharObject firstSelected;

    public static int currentWord;
    public static string thisWord;
    public static AIBehaviour main;

    public Image image;
    public static int category;

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        movements = 0;
        moveCount.text = "Langkah : " + movements;
        ScrambleCategory(category);
        ShowScramble(currentWord);

        index1 = 0;
        //CoArrangeWord();
        //ScanWord();
    }

    // Update is called once per frame
    void Update()
    {
        //ScanWord();


        RepositionObject();
        moveCount.text = "Langkah : " + movements;
    }

    void RepositionObject()
    {
        if (charObjects.Count == 0)
        {
            return;
        }

        float center = ((charObjects.Count - 1) / 2);
        for (int i = 0; i < charObjects.Count; i++)
        {
            charObjects[i].rectTransform.anchoredPosition =
                Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
                new Vector2((i - center) * space, 0), lerpSpeed * Time.deltaTime);
            charObjects[i].index = i;
        }
    }

    // show random word to the screen
    public void ShowScramble()
    {
        ShowScramble(Random.Range(0, words.Length - 1));
    }

    // show random word to the screen
    public void ScrambleCategory(int wordCategory)
    {
        if (wordCategory == 1)
        {
            currentWord = 6;
        }
        else
        {
            currentWord = 0;
        }
    }

    // show word from collection with certain index
    public void ShowScramble(int index)
    {
        charObjects.Clear();
        foreach (Transform child in container)
        {
            Destroy(child.gameObject);
        }

        if (index > words.Length - 1)
        {
            Debug.LogError("Index out of range, please enter range between 0 - " +
                (words.Length - 1).ToString());
            return;
        }

        char[] chars = words[index].GetString().ToCharArray();
        foreach (char c in chars)
        {
            CharObject clone = Instantiate(prefab.gameObject).GetComponent<CharObject>();
            clone.transform.SetParent(container);

            charObjects.Add(clone.Init(c));
        }

        currentWord = index;
        ChangeImage();

        //CoArrangeWord();
        index1 = 0;
        //ScanWord();
    }

    public void Swap(int indexA, int indexB)
    {
        CharObject tempA = charObjects[indexA];

        charObjects[indexA] = charObjects[indexB];
        charObjects[indexB] = tempA;

        //Debug.Log("charObjects tempA : " + tempA.text.text);
        //Debug.Log("charObjects[indexA] : " + charObjects[indexA].text.text);
        //Debug.Log("charObjects[indexB] : " + charObjects[indexB].text.text);

        charObjects[indexA].transform.SetAsLastSibling();
        charObjects[indexB].transform.SetAsLastSibling();

        movements++;

        CheckWord();
    }

    public void Select(CharObject charObject)
    {
        if (firstSelected)
        {
            Swap(firstSelected.index, charObject.index);
            //movements += 1;

            //unselect
            firstSelected.Select();
            charObject.Select();

        }
        else
        {
            firstSelected = charObject;

        }

    }

    public void UnSelect()
    {
        firstSelected = null;
    }

    public void CheckWord()
    {
        StartCoroutine(CoCheckWord());
    }

    IEnumerator CoCheckWord()
    {
        yield return new WaitForSeconds(waitChecking);

        string word = "";
        foreach (CharObject charObject in charObjects)
        {
            word += charObject.character;
        }

        if (word == words[currentWord].word)
        {
            if (currentWord == 5)
            {
                Debug.Log("Index Fruit and Veggie reached limit");
            }
            else
            {
                currentWord++;
                ShowScramble(currentWord);
                movements = 0;
            }
        }
        else
        {
            if (index1 < words[currentWord].word.Length)
            {
                index1++;
            }
            else
            {
                index1 = 0;
            }

            //CoArrangeWord();
            ScanWord();
        }
    }

    void ChangeImage()
    {
        if (category == 1)
        {
            image.sprite = Resources.Load<Sprite>("Hewan/" + words[currentWord].word);
        }
        else
        {
            image.sprite = Resources.Load<Sprite>("Buah/" + words[currentWord].word);
        }
        Debug.Log(words[currentWord].word);
    }

    private void AIScanner()
    {
        string Scramble = "";

        foreach (CharObject charObject in charObjects)
        {
            Scramble += charObject.character;
        }

        //while (Scramble != words[currentWord].word)
        //{

        //}

    }

    public void ScanWord()
    {
        StartCoroutine(CoArrangeWord());
    }

    private int index1;
    IEnumerator CoArrangeWord()
    {
        yield return new WaitForSeconds(3f);

        string Scramble = "";

        foreach (CharObject charObject in charObjects)
        {
            Scramble += charObject.character;
        }

        Debug.Log("Scramble : " + Scramble);

        char[] charsScramble = Scramble.ToCharArray();
        char[] charsResult = words[currentWord].word.ToCharArray();

        for (int i = index1; i < charsResult.Length; i++)
        {
            if (charsScramble[i] == charsResult[index1])
            {
                if (index1 != i)
                {
                    if (charsScramble[index1] != charsScramble[i])
                    {
                        Swap(index1, i);
                        Scramble = "";
                        foreach (CharObject charObject in charObjects)
                        {
                            Scramble += charObject.character;
                        }
                        Debug.Log(Scramble);
                        break;
                    }
                }
                else
                {
                    index1++;
                    ScanWord();
                    break;
                }

            }
        }
    }
}

